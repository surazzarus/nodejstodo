const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const todoController = require('./controllers/todoController');

const port = process.env.PORT || 8080;

// Mongodb Connection
const uri = 'mongodb://localhost/todoList';
mongoose.connect(uri).then(() => {
    console.log('Connected to Mongodb database')
},
err => {
    console.log(err);
});

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// Serving static files
app.use(express.static('public'));

todoController(app);

// Error handling
app.use((err, req, res, next) => {
    res.status(422).send(err.message);
});

app.listen(port, () => {
    console.log(`Listening on port ${port}`)
});