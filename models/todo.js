const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const TodoSchema = new Schema({
    name: {
        type: String,
        required: [true, "Name field is required, it is important"]
    }
});

const Todo = mongoose.model('Todo', TodoSchema);

module.exports = Todo;