const Todo = require('../models/todo');

module.exports = function(app) {
    app.get('/todo', (req, res) => {
        Todo.find({}).then((todos) => {
            res.send(todos)
        })
    });

    app.post('/todo', (req, res, next) => {
        Todo.create(req.body).then((todo) => {
            res.send(todo);
        }).catch(next)
    });

    app.delete('/todo/:id', (req, res, next) => {
        Todo.findByIdAndRemove({_id: req.params.id}).then((todo) => {
            res.send(todo)
        })
    });

    app.put('/todo/:id', (req, res, next) => {
        Todo.findOneAndUpdate({_id: req.params.id}, req.body).then(() => {
            Todo.findOne({_id: req.params.id}).then((todo) => {
                res.send(todo)
            })
        })
    })
};